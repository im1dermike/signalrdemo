﻿using SignalRDemo.DataLayer;
using SignalRDemo.Hubs;
using SignalRDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SignalRDemo.ApiControllers
{
    public class UsersController : ApiController
    {
        public HttpResponseMessage Post(HttpRequestMessage request, UserViewModel viewModel)
        {
            var userRepository = new UserRepository();
            var user = userRepository.Update(viewModel);
            UserHub.UserUpdate(user);
            return request.CreateResponse(HttpStatusCode.OK, viewModel);
        }
    }
}