﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace SignalRDemo.Hubs
{
    [HubName("chat")]
    public class ChatHub : Hub
    {
        private static int userCount = 0;
        private const string secretGroup = "SecretGroup";

        public void Send(string name, string message)
        {
            Clients.All.notify(name, message);
        }

        public void SendToGroup(string name, string message)
        {
            Clients.Group(secretGroup).notify(name, message);
        }

        public void JoinGroup()
        {
            Groups.Add(Context.ConnectionId, secretGroup);
        }

        public void LeaveGroup()
        {
            Groups.Remove(Context.ConnectionId, secretGroup);
        }

        public override System.Threading.Tasks.Task OnConnected()
        {
            userCount++;
            Clients.All.updateUserCount(userCount);
            return base.OnConnected();
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            userCount--;
            Clients.All.updateUserCount(userCount);
            return base.OnDisconnected(stopCalled);
        }
    }
}