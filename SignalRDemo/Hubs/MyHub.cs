﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRDemo.Hubs
{
    public class MyHub : Hub
    {
        public void Broadcast(string message)
        {
            Clients.Caller.notify("I said: " + message);
            Clients.Others.notify("Someone said: " + message);
        }
    }
}