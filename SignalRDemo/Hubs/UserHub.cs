﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using SignalRDemo.Models;

namespace SignalRDemo.Hubs
{
    public class UserHub : Hub
    {
        private static IHubContext context = GlobalHost.ConnectionManager.GetHubContext<UserHub>();

        public static void UserUpdate(UserViewModel user)
        {
            context.Clients.All.onUserUpdate(user);
        }
    }
}